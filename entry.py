import requests
import skilletlib
import time

def entry_func(controller):
    g = requests.get("https://google.com")
    print(str(g))
    print(skilletlib)
    controller.info("info")
    controller.warning("warning")
    controller.error("error")
    controller.debug("debug")

def entry_func_with_args(controller, input_one, test_box, from_dd, input_three, input_four, input_five):
    print(f"Input one {input_one}")
    answers = controller.get_input([
        {
            "name": "input_two",
            "description": f"Asking you this from the script ({input_one})",
            "type": "text"
        }
    ])
    input_two = answers["input_two"]
    print(f"Input two {input_two}")
    print(f"from_dd {from_dd}")
    print(f"from_test_box {test_box}")
    print(f"Input three {input_three}")
    print(f"Input four {input_four}")
    print(f"Input five {input_five}")

def run_forever(controller):
    for i in range(100):
        print(f"Running - {i}")
        time.sleep(1)
